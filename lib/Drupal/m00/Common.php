<?php

/**
 * @file
 * Constants common to the entire module.
 * 
 * Promotes robust Don't Repeat Yourself (DRY) code rather
 * than error-prone Write Everything Twice (WET) code.
 * 
 * Any constant that is used in more than one file SHOULD be defined here 
 * (or should be similarly defined in another shared class) !
 * 
 * This makes it easier to adapt the entire module code to other modules.
 */

namespace Drupal\m00;

class Common {

  /**
   * The module machine name.
   * @var string
   */
  const MODULE = 'm00';

  /**
   * The module display name.
   * 
   * Typically this will contain some upper case characters,
   * but for this template module project it is left the
   * same as the machine name to afford easy subsitution.
   * 
   * @var string
   */
  const MODULE_NAME = 'm00';

  /**
   * The module page path base stem.
   * 
   * @var string
   */
  const STEM = 'm00';
  
  /**
   * Content access permission.
   * 
   * @var string
   */
  const ACCESS_CONTENT = 'access m00 content';
  
  /**
   * User forms access permission.
   * 
   * @var string
   */
  const ACCESS_USER_FORMS = 'access m00 user forms';

  /**
   * Name of admin configuration variable.
   * 
   * @var string
   */
  const VAR_ADMIN = 'm00_var_admin';

  /**
   * Name of user form variable.
   * 
   * @var string
   */
  const VAR_USER = 'm00_var_user';
  
  /**
   * Name of an example theme string variable.
   * 
   * @var string
   */
  const THEME_VAR_INFO = 'info';
  
  /**
   * Name of an example theme simple variable.
   * 
   * @var string
   */
  const THEME_VAR_SIMPLE = 'var_simple';
  
  /**
   * Name of an example theme array variable.
   * 
   * @var string
   */
  const THEME_VAR_ARRAY = 'var_array';
  
}
