<?php

/**
 * @file
 * 
 * For admin forms and their handlers (and supporting functions).
 */

use Drupal\m00\Common;

/**
 * Admin form function called by drupal_get_form() as specified in m00_menu().
 * 
 * Note that no explicit submit handler is required because
 * system_settings_form($form) is returned.
 * 
 * (A validation handler is provided below.)
 * 
 * @param array $form
 *   A Form API form array.
 * @param array $form_state
 *   A Form API form state array.
 */
function m00_form_admin($form, &$form_state) {

  // Define a text input field
  $form[Common::VAR_ADMIN] = array(
    '#type' => 'textfield',
    '#title' => t('An admin configuration variable.'),
    '#default_value' => variable_get(Common::VAR_ADMIN, 3),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('A positive integer variable.'),
    '#required' => TRUE,
  );

  // Take care of providing a submit button and user messages.
  return system_settings_form($form);
  // 'This function adds a submit handler and a submit button to a form array.
  //  The submit function saves all the data in the form, using variable_set(),
  //  to variables named the same as the keys in the form array.'
}

/**
 * Implements validation from the Form API.
 *
 * @param array $form
 *   A structured array containing the elements and properties of the form.
 * @param array $form_state
 *   An array that stores information about the form's current state
 *   during processing.
 */
function m00_form_admin_validate($form, &$form_state) {

  $val = $form_state['values'][Common::VAR_ADMIN];

  if (!is_numeric($val)) {
    form_set_error(Common::VAR_ADMIN, t('The value must be an integer.'));
  }
  elseif ($val <= 0) {
    form_set_error(Common::VAR_ADMIN, t('The value must be positive.'));
  }

}
