<?php

use Drupal\m00\Common;

/**
 * @file
 * For non-admin (user) forms and their handlers (and supporting functions).
 */

/**
 * Builds a themeable user form for a user variable.
 * 
 * @param array $form
 *   A Form API form array.
 * @param array $form_state
 *   A Form API form state array.
 * 
 * @return array
 *   A Form API array for user variable.
 */
function m00_form_user_themed($form, &$form_state) {

  // Define a text input field
  $form[Common::VAR_USER] = array(
    '#type' => 'textfield',
    '#title' => t('A user variable.'),
    '#default_value' => variable_get(Common::VAR_USER, 3),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('A positive integer.'),
    '#required' => TRUE,
  );

  // Provide an explicit submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Do NOT return system_settings_form($form) directly it blocks theming
  // (unless you "double up" on the #theme function declaration) !
  // See https://www.drupal.org/node/211015 https://www.drupal.org/node/320398
  // But if you don't use system_settings_form($form) you will have
  // to provide your own submit handler to persist the new values.

  return $form;
}

/**
 * Themes a form's user variable by decorating the title and description.
 * 
 * The title is prefixed with 'Title: ', wrapped in an <em> and <p> tag,
 * removed from the form array, and prepended to the processed form.
 * 
 * The description is decorated with the prefix 'Description: '
 * through interception and rendered as part of the form array.
 * 
 * @param array $vars
 *   A themeable array for a form.
 * @return string
 *   A rendered form string.
 */
function theme_m00_form_user_themed($vars) {

  $form = $vars['form'];
  
  $title = $form[Common::VAR_USER]['#title'];
  $form[Common::VAR_USER]['#title'] = '';
  $output = '<p><em>Title: ' . $title . '</em></p>';
  
  $form[Common::VAR_USER]['#description'] = 
      'Description: ' . $form[Common::VAR_USER]['#description'];
  
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Validation hander for a themed user form.
 *
 * @param array $form
 *   A structured array containing the elements and properties of the form.
 * @param array $form_state
 *   An array that stores information about the form's current state
 *   during processing.
 */
function m00_form_user_themed_validate($form, &$form_state) {

  $val = $form_state['values'][Common::VAR_USER];

  if (!is_numeric($val)) {
    form_set_error(Common::VAR_USER, t('The value must be an integer.'));
  }
  elseif ($val <= 0) {
    form_set_error(Common::VAR_USER, t('The value must be positive.'));
  }
}

/**
 * Submit handler for a themed user form.
 *
 * Issues a message with the value of the new, validated user variable
 * and writes it to database.
 * 
 * @param array $form
 *   Form API form array.
 * @param array $form_state
 *   Form API form state array.
 */
function m00_form_user_themed_submit($form, &$form_state) {
  $var_user = $form_state['values'][Common::VAR_USER];
  drupal_set_message(t('You chose %input', array('%input' => $var_user)));
  variable_set(Common::VAR_USER, $var_user);
  
  // Variable substitution options:
  // 
  // @variable: Escaped to HTML using check_plain().
  // 
  // %variable: Escaped to HTML (using check_plain()) and formatted using
  // drupal_placeholder(), which makes it display as <em>emphasized</em> text.
  // 
  // !variable: Inserted as is, with no sanitization or formatting.
  
 }
