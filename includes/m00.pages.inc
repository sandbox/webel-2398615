<?php

/**
 * @file
 * Page callbacks of menu items (and supporting functions).
 */
use Drupal\m00\Common;

/**
 * Custom page callback function, declared in hook_menu(), for a welcome page.
 * 
 * @return array
 *   A Drupal page render array (no explicit theming).
 */
function m00_page() {

  // Page render array.
  $page['info'] = array(
    '#markup' => t('Markup for the !module welcome page.', array('!module' => Common::MODULE_NAME)),
    '#prefix' => '<em>',
    '#suffix' => '</em>',
  );

  return $page;
}

/**
 * Custom page callback function for a page with a nested menu item.
 * 
 * @return array
 *   A Drupal page render array (no explicit theming).
 */
function m00_page_nested() {

  // Page render array.
  $page['info'] = array(
    '#markup' => t('Markup for a page with a nested menu item.'),
    '#prefix' => '<em>',
    '#suffix' => '</em>',
  );

  return $page;
}

/**
 * Custom page callback function for a basic unthemed stub page.
 * 
 * @return array
 *   A Drupal page render array (no explicit theming).
 */
function m00_page_basic() {

  // Build a page render array: https://www.drupal.org/node/930760.
  $page = array();

  $page['info'] = array(
    '#markup' => t('Markup for an unthemed page that echos database variables.'),
    // Where possible keep HTML out of the translatable text.
    '#prefix' => '<p><em>',
    '#suffix' => '</em></p>',
      // The markup will also be wrapped in an <em> then a <p>.
  );

  $page[Common::VAR_ADMIN] = array(
    '#markup' => t(
        'The integer config variable has the value: !value', array('!value' => variable_get(Common::VAR_ADMIN, 0),)
    ),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
      // The markup will be wrapped in a <p>.    
  );

  $page[Common::VAR_USER] = array(
    '#markup' => t(
        'The integer user variable has the value: !value', array('!value' => variable_get(Common::VAR_USER, 0),)
    ),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
      // The markup will be wrapped in a <p>.    
  );

  return $page;
}

/**
 * Custom themed page callback function, declared in m00_menu().
 * 
 * @return array
 *   A Drupal page render array.
 */
function m00_page_themed() {

  // The name of this function.
  $i = Common::MODULE . '_page_themed';

  // Add data properties (theme variables) for theming, with no HTML anywhere.
  // They have to match those defined in the hook_theme() in the .module file.
  // Note that theme vars are stored with a '#' prefix but extracted without.

  $data['#' . Common::THEME_VAR_INFO] = t(
      "Themed by '!theme_function', which formats a simple value and an array value.", array('!theme_function' => 'theme_' . $i)
  );

  $data['#' . Common::THEME_VAR_SIMPLE] = 'simple';

  $data['#' . Common::THEME_VAR_ARRAY] = array(
    'kid1',
    'kid2',
  );

  $data['#theme'] = array($i);

  return $data;
}

/**
 * Themes an example page.
 * 
 * @param array $vars
 *   An array with 'info', 'var_simple', and 'var_array' keys.
 * @return string
 *   A rendered, themed Drupal page.
 */
function theme_m00_page_themed($vars) {

  // Build a Drupal render array: https://www.drupal.org/node/930760.
  $page = array();

  $info = $vars[Common::THEME_VAR_INFO];
  $page[Common::THEME_VAR_INFO] = array(
    '#markup' => $info,
    '#prefix' => '<p><em>',
    '#suffix' => '</em></p>',
      // Markup will be wrapped in an <em> then a <p>.
  );

  $var_simple = $vars[Common::THEME_VAR_SIMPLE];
  $page[Common::THEME_VAR_SIMPLE] = array(
    '#markup' => '<b>' . Common::THEME_VAR_SIMPLE . ':</b> ' . $var_simple,
    '#prefix' => '<p>',
    '#suffix' => '</p>',
      // Markup will be wrapped in a <p>.
  );

  $var_array = $vars[Common::THEME_VAR_ARRAY];
  $page[Common::THEME_VAR_ARRAY] = array(
    '#prefix' => '<div>',
    '#suffix' => '</div>',
      // We can't use a <p> here because it has to contain an item_list <div>.
  );
  $page[Common::THEME_VAR_ARRAY]['title'] = array(
    '#markup' => '<b>' . Common::THEME_VAR_ARRAY . ':</b>',
  );
  // For the array var list use a Drupal theme function.
  $page[Common::THEME_VAR_ARRAY]['kids'] = array(
    '#markup' => theme('item_list', array('items' => $var_array))
  );

  return drupal_render($page);
}
