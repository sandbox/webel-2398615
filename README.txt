This README.txt is adapted from the Drupal.org README Template:

 * https://www.drupal.org/node/2181737

IMPORTANT: unlike all other files in this project, this README.txt can't
be simply adapted for use in another project through replacement of the
string 'm00' with your module name, you must completely rewrite this file.


INTRODUCTION
------------

A template project designed as a starting point for other contributed modules.

It aims to provide one of each of the features and capabilities you might
require for contributed modules, ready to be adapted for a real project.

The project name 'M00' and machine name 'm00' are chosen to afford easy
find-and-replace substitution, where the 'm' stands for 'module'.

To adapt for you project, simply clone then replace the string 'm00' everywhere
with your project/module machine name:

- (TODO sed/awk/perl module machine name replacement examples).

This is an actively maintained and evolving tutorial development project,
with a live demonstration site home page:

 * http://drupal7demo.webel.com.au/module/m00


REQUIREMENTS
------------

This module depends on X Autoload, which is highly recommended
for object-oriented projects or any project using any Classes:

 * X Autoload (http://drupal.org/project/xautoload)

This is included in the .info file as a starting point for dependencies.


INSTALLATION
------------

 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * To see the stub block just activate it under the block list for your theme
   at /admin/structure/block.

 * To view the stub page enable the 'access m00 content' permission
   at /admin/people/permissions.


CONFIGURATION
-------------

 * There is currently only one configuration parameter, a
   positive integer variable that is otherwise not used,
   except for display on the main page /m00. Configure it at:

   /admin/config/development/m00


ISSUE REPORTING
---------------

Drupal.org (sandbox) project page:
  https://drupal.org/sandbox/webel/2398615

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/2398615


MAINTAINERS
-----------

webel - http://drupal.org/user/392755

No other maintainers are currently sought.


CREDITS
-------

The M00 project/module template is developed by Webel IT Australia,
specialists in PHP-driven Drupal CMS web engineering, UML, SysML, Java and XML.
In addition to using Drupal CMS for developing clients' web sites, Webel has
used Drupal CMS for many years to develop educational web sites promoting 
graphical software engineering with Unified Modeling Language (UML) and 
graphical systems engineering with Systems Modeling Language (SysML).

  http://www.webel.com.au

  http://drupal7demo.webel.com.au


SPONSORS SOUGHT
---------------

If you are interesting in seeing M00 expanded to become a more substantial
template for Drupal7 module development, or in seeing a Drupal8 version
developed, please consider supporting the project.

Donations to the M00 project may be made online via PayPal at:

  http://drupal7demo.webel.com.au/lm_paypal/donations